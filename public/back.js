import { ajax, timeout } from "../utils/util.js";

//当前后台页的js入口
function backjs() {
  //获取当前页信息
  chrome.tabs.getSelected(null, (tab) => {
    chrome.tabs.executeScript(null, {
      code: `
        var kw = document.querySelector("#kw");
        console.log(kw.value);
      `,
    });
  });
}

//右键菜单
chrome.contextMenus.create({
  title: "新增按钮",
  onclick: () => {
    console.log("点击新增按钮");
  },
  //匹配网站
  documentUrlPatterns: ["*://*.tucao.one/*"],
  //匹配 可编辑控件/选中内容
  contexts: ["editable", "selection"],
});

//监听C站所有请求 加快访问速度
chrome.webRequest.onBeforeRequest.addListener(
  function (info) {
    const url = info.url;
    if(url.includes('www.tucao.one')) {//吐槽
      if (
        url.includes("/ol.php") ||
        url.includes("/api/today_add.php")
      ) {
        return {
          redirectUrl: chrome.extension.getURL("public/tucao/json/1.json") || "",
        };
      } else if (url.includes("/player_test/player.js")) {
        return {
          redirectUrl: chrome.extension.getURL("public/tucao/js/player.js") || "",
        };
      }

    } else if (url.includes("//www.pangci.cc/index.php")) {//胖次
      return {
        redirectUrl: chrome.extension.getURL("public/tucao/json/1.json") || "",
      };
    }
  },
  {
    urls: [
      "*://www.tucao.one/*",
      "*://bdimg.share.baidu.com/static/api/js/share.js*",
      "*://www.pangci.cc/*",
    ],
  }, //监听页面请求,你也可以通过*来匹配。
  ["blocking"]
);

//取最新版本号
function getNewVersion() {
  // $.ajax({
  //   url: "https://www.cnblogs.com/wuhairui/p/14466831.html",
  //   type: "get",
  //   success: (data) => {
  //     const s =
  //       '<div id="cnblogs_post_body" class="blogpost-body blogpost-body-html">';
  //     const e = '<div id="MySignature"></div>';
  //     const si = data.indexOf(s);
  //     const ei = data.lastIndexOf(e);
  //     data = data.substring(si + s.length, ei - e.length);
  //     const newVersion =
  //       $(`<data>${data}</data>`)?.find("#edition")?.text() || "";
  //     chrome.storage.local.set({ newVersion: newVersion }, () => {});
  //   },
  //   error: (err) => {},
  // });
}

//取当前版本号
function getVersion() {
  $.ajax({
    url: "../manifest.json",
    type: "get",
    success: (data) => {
      chrome.storage.local.set({ version: data.version }, () => {});
    },
    error: (err) => {},
  });
}
getVersion();
getNewVersion();
const timer = setInterval(() => {
  getNewVersion();
}, 60000);
