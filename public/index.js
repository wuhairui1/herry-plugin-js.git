import { ajax, getChromeStorage, setChromeStorage } from "../utils/util.js";
import "./build/herryPostMessage.js";

// 链接列表;
const links = [
  {
    url: "https://www.bilibili.com/",
    text: "B站首页",
  },
  {
    url: "https://www.tucao.one/",
    text: "C站首页",
  },
];


//广告开关设置组件
const AdvertSetup = {
  data() {
    return {
      adSetup: true,//广告开关
    }
  },
  async mounted() {
    const res = await getChromeStorage('adSetup');
    if([null, undefined, ''].includes(res)) {
      this.adSetup = true;
      return;
    }
    this.adSetup = res;
  },
  methods: {
    //广告开关设置
    async setAd() {
      this.adSetup = !this.adSetup;
      await setChromeStorage({adSetup: this.adSetup});
      alert('请刷新C站页面')
    },
  },
  template: `
    <div class="advertSetup">
      C站广告开关设置：
      <b class="btn" @click="setAd">{{adSetup ? '开' : '关'}}</b>
    </div>
  `,
}

//B站设置项
const BilibiliSetup = {
  components: {
    AdvertSetup,
  },
  template: `
    <div class="setup">
      <h1>B站设置项：</h1>
      待加入
    </div>
  `,
};

//C站设置项
const TucaoSetup = {
  components: {
    AdvertSetup,
  },
  template: `
    <div class="setup">
      <h1>C站设置项：</h1>
      <AdvertSetup />
    </div>
  `,
};

/**
 * 主程序
 */
const app = Vue.createApp({
  components: {
    BilibiliSetup,
    TucaoSetup,
  },
  data() {
    return {
      links,
    };
  },
  mounted() {},
  methods: {},
  template: `
    <div>
      <div id="link">
        <a :href="link.url" v-for="(link) in links" class="link" target="_blank">{{link.text}}</a>
      </div>
      <BilibiliSetup />
      <TucaoSetup />
    </div>
  `,
});

const vm = app.mount("#app");
