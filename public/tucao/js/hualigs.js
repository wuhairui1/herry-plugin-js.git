init();
//timeout
function timeout(t) {
    return new Promise((success) => {
        setTimeout(() => {
            success();
        }, t);
    });
}

async function init() {
    await timeout(1000);
}

$("#Hidove").on('change', async function () {
    await timeout(500);
    const $uploadBtn = $('.kv-file-upload.btn-sm.btn-kv.btn-outline-secondary')
    if ($uploadBtn.length > 0) {
        $uploadBtn.click();
        await timeout(500);
        const urlTimer = setInterval(() => {
            if ($("#urlcode .form-control").length > 0) {
                herry.returnData('getUploadImg', $("#urlcode .form-control").val());
                clearInterval(urlTimer)
            }
        }, 1000)
    }
});