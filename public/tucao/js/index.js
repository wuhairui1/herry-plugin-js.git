window.onload = function () {
  init();
};

async function init() {
  const { pathname, search, hostname } = location;
  if (hostname == 'www.tucao.one') {
    //吐槽网
    if (pathname.includes("/play/h")) {
      //视频详情页
      await createjs("public/build/jquery.min.js");
      await createjs("public/build/hls.min.js");
      await createjs("public/build/herryPostMessage.js");
      // await createjs("public/tucao/js/tucaoDetail.js");
    } else if (search.includes("?m=member&c=content")) {
      //投稿页
      await createjs("public/build/herryPostMessage.js");
      await createjs("public/tucao/js/tougaopage.js");
    }
  } else if (hostname == 'www.hualigs.cn') {
    //遇见图床网
    await createjs("public/build/herryPostMessage.js");
    await createjs("public/tucao/js/hualigs.js");
  }
}

//chrome对象存入缓存
localStorage.setItem(
  "localData",
  JSON.stringify({
    path: chrome.extension.getURL("/") || "", //插件主路径
  })
);
