/**
 * 投稿页脚本
 */
//取localData
const localData = JSON.parse(localStorage.getItem("localData")) || {};
//图片框默认图片换掉
if($("#thumb").val() == '') {
    $("#thumb_preview").attr('src',`${localData.path}public/tucao/img/uploadImg.jpg`)
}
//上传封面逻辑替换
$("#myform .form_table .upload-pic a").attr("onclick",`null`).on('click',() => {
    console.log('上传图片')
    aui_close();
    //模拟上传
    $("body").append(`
        <script src="${localData.path}public/build/herryPostMessage.js"></script>
        <div class="hualigs">
            <div class="aui_titleBar">
                <div class="aui_title" style="cursor: move; display: block;">封面上传</div>
                <a class="aui_close" style="display: block;" onclick="aui_close()">×</a>
            </div>
            <iframe id="hualigs" src="https://www.hualigs.cn/" style="display:block;"></iframe>
        </div>
    `);
    herry.iframeId = "hualigs";
    //上传回调
    herry.postMessage({ action: "getUploadImg" }, (res) => {
        if(res.data) {
            aui_close();
            $("#thumb_preview").attr('src',res.data);
            $("#thumb").val(res.data);
        }
    });
});

function aui_close() {
    $(".hualigs").remove();
}