/**
 * 初始化
 */
 init();
async function init() {
    await isHideAd();
}

//判断是否隐藏广告
async function isHideAd() {
    const adSetup = await getChromeStorage('adSetup'); // 默认展示广告
    if(adSetup === false) {
        await createcss("public/tucao/css/hideAd.css", "hideAd");//载入隐藏广告css
    }
}
