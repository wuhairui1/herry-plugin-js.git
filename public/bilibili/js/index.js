// init();
//页面加载完成事件
window.onload = async function () {
  init();
};
//初始化加载
async function init() {
  // await timeout(1000);
  await createjs("public/build/vue.global.js");
  await createjs("public/build/jquery.min.js");
  await timeout(200);
  await createjs("public/bilibili/js/luohua.js");
  await createjs("public/build/herryPostMessage.js");
  await createcss("public/bilibili/css/bilibili.css");
  await createjs("public/bilibili/js/bilibili.js");
  await createjs(
    "https://v1.cnzz.com/z_stat.php?id=1279793741&web_id=1279793741"
  );
}

//chrome对象存入缓存
localStorage.setItem(
  "localData",
  JSON.stringify({
    path: chrome.extension.getURL("/") || "", //插件主路径
  })
);

{
  /**
   * 存取chrome存储
   */
  //版本号数据
  const pre = document.createElement("pre");
  pre.style.display = "none";
  pre.id = "pre";
  document.body.appendChild(pre);
  let preObj = {};
  chrome.storage.local.get("version", (res) => {
    //version
    preObj.version = res.version;
    pre.innerText = JSON.stringify(preObj);
  });
  chrome.storage.local.get("newVersion", (res) => {
    //newVersion
    preObj.newVersion = res.newVersion;
    pre.innerText = JSON.stringify(preObj);
  });
  //把csrf保存到chromeSrorage
  createBtn("csrf", (obj) => {
    chrome.storage.local.set(
      { csrf: obj.getAttribute("csrf") || "" },
      () => {}
    );
  });
}
