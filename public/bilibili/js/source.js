/**
 * 跨域交互
 */
// import { GetUrlQueryString, timeout } from "../../../utils/util.js";
import "../../build/jquery.min.js";
import "../../build/herryPostMessage.js";

herry.callback = async (data) => {
  if (data.action === "getScList") {
    //取收藏列表
    const { csrf } = data;
    const id = await getId(csrf);
    await getContent(id);
  } else if (data.action === "setScList") {
    //写入收藏列表
    const { csrf, scList } = data;
    const id = await getId(csrf);
    await setContent(id, csrf, scList);
  }
};

//获取我的草稿箱中是个有我的学习列表专栏 并取得id
async function getId(csrf) {
  let id = 0;
  const resolve = await fetch("https://member.bilibili.com/x/web/draft/list", {
    headers: {
      accept: "application/json, text/javascript, */*; q=0.01",
      "accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
      "cache-control": "no-cache",
      pragma: "no-cache",
      "sec-ch-ua":
        '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
      "sec-ch-ua-mobile": "?0",
      "sec-fetch-dest": "empty",
      "sec-fetch-mode": "cors",
      "sec-fetch-site": "same-origin",
      "x-requested-with": "XMLHttpRequest",
    },
    referrer: "https://member.bilibili.com/platform/upload/text/edit",
    referrerPolicy: "strict-origin-when-cross-origin",
    body: null,
    method: "GET",
    mode: "cors",
    credentials: "include",
  });
  const data = await resolve.json();
  if (data.artlist) {
    const { drafts } = data.artlist || [];
    for (let i = 0; i < drafts.length; i++) {
      if (drafts[i].title === "我的学习列表") {
        id = drafts[i].id;
      }
    }
    //若没有这个专栏 创建
    if (id === 0) {
      const id = await createColumn(csrf);
      return id;
    }
    return id;
  }
}

//获取我的学习列表的内容
async function getContent(aid) {
  const resolve = await fetch(
    "https://api.bilibili.com/x/article/creative/draft/view?aid=" + aid,
    {
      headers: {
        accept: "application/json, text/javascript, */*; q=0.01",
        "accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
        "cache-control": "no-cache",
        pragma: "no-cache",
        "sec-ch-ua":
          '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
        "sec-ch-ua-mobile": "?0",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-site",
      },
      referrer: "https://member.bilibili.com/",
      referrerPolicy: "strict-origin-when-cross-origin",
      body: null,
      method: "GET",
      mode: "cors",
      credentials: "include",
    }
  );
  const data = await resolve.json();
  if (data.data) {
    let { content = "" } = data.data;
    $("#data").html(content);
    content = content
      .replace("<p>", "")
      .replace("</p>", "")
      .replace(/&#34;/g, '"');
    herry.returnData('getScList', eval(content));
  } else {
    console.log("读取服务器,失败");
  }
}
//将新增数据写入我的学习列表
async function setContent(aid, csrf, scList) {
  const resolve = await fetch(
    "https://api.bilibili.com/x/article/creative/draft/addupdate",
    {
      headers: {
        accept: "application/json, text/javascript, */*; q=0.01",
        "accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
        "cache-control": "no-cache",
        "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        pragma: "no-cache",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-site",
      },
      referrer: "https://member.bilibili.com/",
      referrerPolicy: "strict-origin-when-cross-origin",
      body: `title=${encodeURIComponent(
        "我的学习列表"
      )}&banner_url=&content=${encodeURIComponent(
        JSON.stringify(scList)
      )}&words=120&category=34&list_id=0&tid=4&reprint=0&tags=&image_urls=&origin_image_urls=&dynamic_intro=&media_id=0&spoiler=0&original=0&top_video_bvid=&aid=${aid}&csrf=${csrf}`,
      method: "POST",
      mode: "cors",
      credentials: "include",
    }
  );
  const data = await resolve.json();
  if (data.code === 0) {
    herry.returnData('setScList', true);
  } else {
    herry.returnData('setScList', false);
  }
}
//创建专栏
async function createColumn(csrf) {
  const resolve = await fetch(
    "https://api.bilibili.com/x/article/creative/draft/addupdate",
    {
      headers: {
        accept: "application/json, text/javascript, */*; q=0.01",
        "accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
        "cache-control": "no-cache",
        "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        pragma: "no-cache",
        "sec-ch-ua":
          '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
        "sec-ch-ua-mobile": "?0",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-site",
      },
      referrer: "https://member.bilibili.com/",
      referrerPolicy: "strict-origin-when-cross-origin",
      body: `title=${encodeURIComponent(
        "我的学习列表"
      )}&banner_url=&content=&summary=&words=0&category=0&tid=4&reprint=0&tags=&image_urls=&origin_image_urls=&dynamic_intro=&media_id=0&spoiler=0&original=0&top_video_bvid=&csrf=${csrf}`,
      method: "POST",
      mode: "cors",
      credentials: "include",
    }
  );
  const data = await resolve.json();
  // console.log(data);
  if (data.code === 0) {
    const { aid = "" } = data.data;
    return aid;
  } else {
    return "";
  }
}

