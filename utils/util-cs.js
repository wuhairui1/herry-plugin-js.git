/**
 * content_scripts使用的公共方法
 */
//加载js
function createjs(path, type = "text/javascript") {
  return new Promise((success) => {
    let js = document.createElement("script");
    js.type = type;
    if (path.includes("http") || path.includes("//")) {
      js.src = path;
    } else {
      js.src = chrome.extension.getURL(path) || "";
    }
    document.body.appendChild(js);
    success();
  });
}
//加载css
function createcss(path, id = "") {
  return new Promise((success) => {
    let css = document.createElement("link");
    css.rel = "stylesheet";
    css.id = id;
    if (path.includes("http")) {
      css.href = path;
    } else {
      css.href = chrome.extension.getURL(path);
    }
    document.head.appendChild(css);
    success();
  });
}
//timeout
function timeout(t) {
  return new Promise((success) => {
    setTimeout(() => {
      success();
    }, t);
  });
}
//ajax
function ajax(url, type = "get", data = null) {
  return new Promise((res, rej) => {
    fetch(url, {
      method: type,
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
      body: !data ? null : JSON.stringify(data),
    })
      .then((Response) => Response.json())
      .then((json) => res(json))
      .catch((err) => rej(err));
  });
}
/*
获取某url中的某参数值
调用:GetUrlQueryString("[url地址]","[参数名]");
*/
function GetUrlQueryString(url, name) {
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
  var r = url.substring(url.indexOf("?")).substr(1).match(reg);
  if (r != null) return unescape(r[2]);
  return null;
}
//将url参数转为json对象
function parseQueryString(str) {
  var arr = [],
    length = 0,
    res = {},
    si = str.indexOf("?");
  str = str.substring(si + 1);
  arr = str.split("&");
  length = arr.length;
  for (var i = 0; i < length - 1; i++) {
    res[arr[i].split("=")[0]] = arr[i].split("=")[1];
  }
  return res;
}
//创建隐藏回调事件按钮 1.按钮的id 2.回调，可传参当前按钮对象
function createBtn(id, fn) {
  let btn = document.createElement("button");
  btn.id = id;
  btn.onclick = () => {
    fn(btn);
  };
  btn.innerText = "插件按钮";
  btn.style.display = "none";
  document.body.appendChild(btn);
}

//取chrome储存
async function getChromeStorage(_name) {
  return new Promise((resolve) => {
    chrome.storage.local.get(_name, (res) => {
      resolve(res[_name]);
    });
  })
}

//设置chrome储存
async function setChromeStorage(keyValue) {
  return new Promise((resolve) => {
    chrome.storage.local.set(keyValue, () => {
      resolve(true)
    });
  })
}