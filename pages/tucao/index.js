/**
 * 吐槽弹幕网
 */
// import { ajax } from "../../utils/util.js";

const app = Vue.createApp({
  data() {
    return {};
  },
  mounted() {},
  template: `
    <div class="center">
      <Recommend />
    </div>
  `,
});

//头部推荐
app.component("Recommend", {
  data() {
    return {
      list: [],
    };
  },
  mounted() {
    $.ajax({
      url: "http://www.tucao.one/",
      type: "get",
      contentType: "text",
      success: (res) => {
        const data = $(`<data>${res}</data>`);
        const $new_tpos = data.find(".new_w .new_tpos ul li"); //头部6个推荐
        // console.log($new_tpos);
        $new_tpos.each((i, li) => {
          // console.log(li)
          let data = {
            url: $(li).find("a").attr("href") || "",
            title: $(li).find("a img").attr("alt") || "",
            img: $(li).find("a img").attr("src") || "",
          };
          console.log(data)
          this.list.push(data);
        });
      },
      error: (err) => {},
    });
  },
  template: `
    <ul class="new_tpos">
      <li>1</li>
      <li>2</li>
    </ul>
  `,
});

const vm = app.mount("#herry");
